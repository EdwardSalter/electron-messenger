import { connect } from 'react-redux';
import TitleBar from '../components/TitleBar';

const mapStateToProps = (state) => ({
  currentProtocol: state.imProtocol
});


export default connect(mapStateToProps)(TitleBar);
