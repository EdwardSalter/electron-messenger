import { whatsapp, messenger, slack } from './components/Svgs';

export default [
  {
    title: 'Messenger',
    url: 'https://www.messenger.com/',
    colour: '#0084ff',
    titleColour: '#fff',
    icon: messenger
  },
  {
    title: 'WhatsApp',
    url: 'https://web.whatsapp.com/',
    colour: '#60d66a',
    titleColour: '#F7F9FA',
    icon: whatsapp
  },
  {
    title: 'Slack',
    url: 'https://slack.com/signin',
    colour: '#fff',
    icon: slack
  }
];
