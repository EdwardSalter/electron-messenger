import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Pane from 'react-desktop/src/navPane/windows/pane/pane';
import { NavPaneItem } from 'react-desktop';
import { ipcRenderer as ipc } from 'electron';
import { Set } from 'immutable';
import cn from 'classnames';
import NotificationSvg from './NoticationSvg';
import styles from './NavPane.css';

const mapStateToProps = (state) => ({
  location: state.router.location || {}
});

const mapDispatchToProps = {
  navigateTo: push
};


class NavPane extends PureComponent {
  static propTypes = {
    location: PropTypes.shape({ pathname: PropTypes.string }),
    navigateTo: PropTypes.func.isRequired,
    navItems: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string,
      colour: PropTypes.string,
      icon: PropTypes.node,
    })),
    className: PropTypes.string,
    children: PropTypes.node,
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedTab: null,
      protocolsWithUnreadMessages: Set()
    };

    ipc.on('msg-received', (e, protocol) => {
      console.log(`msg-received, adding ${protocol} to list of protocols with no messages`);
      const newMessageList = this.state.protocolsWithUnreadMessages.add(protocol.toLowerCase());
      this.setState({ protocolsWithUnreadMessages: newMessageList });
    });

    ipc.on('window-focussed', () => {
      const protocol = (this.state.selectedTab || '').toLowerCase();
      const newMessageList = this.state.protocolsWithUnreadMessages.remove(protocol);
      this.setState({ protocolsWithUnreadMessages: newMessageList });
    });

    ipc.on('msg-cleared', (e, protocol) => {
      const newMessageList = this.state.protocolsWithUnreadMessages.remove(protocol.toLowerCase());
      this.setState({ protocolsWithUnreadMessages: newMessageList });
    });
  }

  onNavItemSelected = (title) => {
    ipc.send('tab-changed', {});
    this.props.navigateTo(`/im/${title}`);

    console.log(title);
    const newMessageList = this.state.protocolsWithUnreadMessages.remove(title.toLowerCase());
    this.setState({ selectedTab: title, protocolsWithUnreadMessages: newMessageList });
  }

  renderNavPaneItem = ({ title, colour, icon }) => {
    let iconEl = React.cloneElement(icon, {
      style: {
        color: colour
      }
    });

    if (this.state.protocolsWithUnreadMessages.includes(title.toLowerCase())) {
      iconEl = <NotificationSvg svg={iconEl} />;
    }

    iconEl = React.cloneElement(iconEl, {
      className: styles.icon
    });

    return (
      <NavPaneItem
        title={title}
        color={colour}
        icon={iconEl}
        selected={this.props.location.pathname === `/im/${title.toLowerCase()}`}
        onSelect={() => { this.onNavItemSelected(title.toLowerCase()); }}
      />
    );
  }

  render() {
    const {
      className, navItems, children, ...rest
    } = this.props;

    return (
      <div className={cn(styles.container, className)}>
        <div
          className={styles.navPane}
        >
          <Pane
            items={navItems.map(this.renderNavPaneItem)}
            {...rest}
          />
        </div>
        <div className={styles.content}>
          {children}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavPane);
