import React from 'react';
import PropTypes from 'prop-types';
import { TitleBar } from 'react-desktop';
import { ipcRenderer as ipc, remote } from 'electron';

class TitleBar2 extends React.PureComponent {
  static propTypes = {
    currentProtocol: PropTypes.shape({
      titleColour: PropTypes.string,
      colour: PropTypes.string,
    }),
  };
  static defaultProps = {
    currentProtocol: {},
  };

  constructor(props) {
    super(props);
    this.state = { isMaximized: remote.getCurrentWindow().isMaximized() };
    ipc.on('windowstate-modified', (e, msg) => {
      switch (msg) {
        case 'restored':
          this.setState({ isMaximized: false });
          break;
        case 'maximized':
          this.setState({ isMaximized: true });
          break;
        default:
          break;
      }
    });
  }

  notifyMainOfWindowState = (windowState) => {
    ipc.send('windowstate-modification-requested', windowState);
  }

  close = () => this.notifyMainOfWindowState('close')
  minimize = () => this.notifyMainOfWindowState('minimize');
  toggleMaximize = () => {
    this.setState({ isMaximized: !this.state.isMaximized });
    this.notifyMainOfWindowState('maximize');
  };

  render() {
    const { isMaximized } = this.state;
    const currentProtocol = this.props.currentProtocol || {};

    return (<TitleBar
      controls
      title=" "
      isMaximized={isMaximized}
      // theme={this.props.theme}
      background={currentProtocol.titleColour || currentProtocol.colour || 'transparent'}
      onCloseClick={this.close}
      onMinimizeClick={this.minimize}
      onMaximizeClick={this.toggleMaximize}
      onRestoreDownClick={this.toggleMaximize}
    />);
  }
}


export default TitleBar2;
