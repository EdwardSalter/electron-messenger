import protocols from '../imProtocols';

export const SWTICH_IM_PROTOCOL = 'SWTICH_IM_PROTOCOL';

export default function switchProtocol(protocolName) {
  const protocol = protocols.find(p => p.title === protocolName);

  return {
    type: SWTICH_IM_PROTOCOL,
    payload: protocol
  };
}
